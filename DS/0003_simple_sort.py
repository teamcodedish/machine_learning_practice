# simple_sort.py
def simple_sort(array):
    n = len(array)
    for i in range(n):
        # compare array[i] with array[i+1], array[i+2], ... array[n-1]
        for j in range(i + 1, n):
            # swap if we find a smaller value
            if array[i] > array[j]:
                array[i], array[j] = array[j], array[i]
    return array


array = [5, 10, 8, 7, 3, 6, 12, 2, 7];
sorted_array = simple_sort(array)
print("Sorted array")
print(sorted_array)
