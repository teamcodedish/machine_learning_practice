import importlib

import matplotlib.pyplot as plt

from v1_dataset import dataset

cost_function_module = importlib.import_module("0002_cost_function")
prediction_function = importlib.import_module("0001_prediction_function")
cost_function = cost_function_module.cost_function
predict = prediction_function.predict

# bias and weight (try out various values!)
b = 15.0  # bias
w1 = 3.0  # weight

# separate xs and ys
xs = [x for x, _ in dataset]
ys = [y for _, y in dataset]

# calculate cost and predictions
cost = cost_function(w1, b)
predictions = [predict(x, w1, b) for x in xs]
print("Value of cost function =", cost)
# plot data points and regression line
plt.scatter(xs, ys, c='red')
plt.plot(xs, predictions, c='blue')
plt.xlim(4.0, 10.0)
plt.ylim(min(0.0, min(predictions)), max(50.0, max(predictions)))
plt.show()
