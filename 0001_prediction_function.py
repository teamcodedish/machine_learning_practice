def predict(x, w, b):
    """
    predict y given input and model parameters
    :param x:
    :type x:
    :param w:
    :type w:
    :param b:
    :type b:
    :return:
    :rtype:
    """
    return x * w + b
