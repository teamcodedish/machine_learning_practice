** Machine learning Notes 
reference link: https://www.commonlounge.com/discussion/33a9cce246d343dd85acce5c3c505009

## What is Machine Learning? Why Machine Learning?
- Discover patterns 
    - learn the patterns from examples. 
    - write an algorithm based on learning 
- Make predictions
    - using algorithm solve the same problem in new situations.

- example
    - recognize handwriting. for ex. recognize 0 or 6 
    - converting speech to text, 
    - checking if an email is spam
    - predicting the price of a house, etc

- summary 
    - what is machine learning ?
    - machine learning used which two things ?   
    - what is difference b/t ml and ai ?

## Linear Regression
- what is regression problems ?

- machine learning models consists of three part ?
    - Representation
        - the model representation 
        - ex. In linear regression, the model representation is a straight line
        - y(x) = w1x1 + b1
    - Evaluation
        - The cost function 
        - "scores" the goodness (or badness) of each possible model
    - Optimization
        - gradient descent
        - finding the best model representation and the cost function.
    
- what the equation  yw,b(x) = w1x1 + b representation ?     
- what are the parameter on the above equation ?    
- what is feature ?
- what is multiple linear regression and whats its equation ? 
- what is Residuals/error ?
- what is the Cost functions and what its formula ?
- what is the Cost functions for  linear regression ?
- what is mean squared error ?
- code for calculating the prediction ?
- code for calculating the cost function ?
- Optimization using Gradient Descent ?
    
- summary 
    - what is regression problems ?
    - machine learning models consists of three part ?
    - what the equation  yw,b(x) = w1x1 + b representation ?     
    - what are the parameter on the above equation ?    
    - what is feature ?
    - what is multiple linear regression and whats its equation ? 
    - what is Residuals/error ?
    - what is the Cost functions and what its formula ?
    - what is the Cost functions for  linear regression ?
    - what is mean squared error ?
    - code for calculating the prediction ?
    - code for calculating the cost function ?
    - Optimization using Gradient Descent ?

## Gradient Descent
- what is gradient descent ?
- what is the procedure of gradient descent ?
    - iterative method. 
    - with initial weights and biases, find the value of the cost function 
    - calculating the gradient 
    - and move in the direction in which the cost function reduces. 
    - By repeating this step thousands of times.
    
- what is the relation b/w the gradient decent, cost function and the model parameter
    - Gi = d(J(w))/dwi

- what is the directional relation b/w gradient and the cost function and gradient decent ?

- Pseudocode for Gradient Descent
    - initialize the weights  w randomly
    - calculate the gradients
        - Gi = d(J(w))/dwi = 2/n sigma(Ypredi- Ytruei)
    - update the weights using learning rate and Gradient decent 
        - w ← w − η⋅G
    - repeat till J(w) stops reducing or some termination criteria is met.

- what is learning rate ?
- what is the visualise gradient descent ?
- what is global vs local minima ?
- what happen if learning rate chosen very small or very high ?
- what are the some method to choose automatically choose a suitable learning rate. ?
    - Adam optimizer
    - AdaGrad 
    - RMSProp
    
- what are Variants of Gradient Descent ?
    - There are multiple variants of gradient descent depending 
        - how much of the data is being used to calculate the gradient.
        - computational efficiency.
        
    - Batch gradient descent
        - finding cost function on entire training data.  
        - gradient descent can be very slow
        
    - mini-batch gradient descent
        - divide the training data into small batches
        - calculate the gradient
        - for each small mini-batch of training data
        
    - Stochastic gradient descent (SGD)
        - computes the gradient
        - each update using a single training data point x(i) (chosen at random
       
        - assume  
            - single data point x = (x1, x2) = (2, -3) and y = 1
            - p = w1x1 + w2x2 + b
            - J(w) = (p-y)**2
            - w1, w2) = (0.1, 0.2) and b = 0 
            - find d(J(w)))/d(w)
            - updated w1, w2, b  = ??
            
- Summary
    - what is gradient descent ?
    - what is the procedure of gradient descent ? 
    - what is the relation b/w the gradient decent, cost function and the model parameter
    - what is the directional relation b/w gradient and the cost function and gradient decent ?
    - Pseudocode for Gradient Descent        
    - what is learning rate ?
    - what is the visualise gradient descent ?
    - what is global vs local minima ?
    - what happen if learning rate chosen very small or very high ?
    - what are the some method to choose automatically choose a suitable learning rate. ?       
    - what are Variants of Gradient Descent ?
    - Batch gradient descent ?
    - mini-batch gradient descent ?
    - Stochastic gradient descent (SGD) ?
    - calculate Stochastic gradient descent the assume ? 
        - single data point x = (x1, x2) = (2, -3) and y = 1
        - p = w1x1 + w2x2 + b
        - J(w) = (p-y)**2
        - w1, w2) = (0.1, 0.2) and b = 0 
        - find d(J(w)))/d(w)
        - updated w1, w2, b  = ??
        
## Hands-on Assignment: Implementing Linear Regression with Gradient Descent
- predict the progression of diabetes in patients ?
    - load the dataset and splitting the dataset
        - load diabetes dataset 
            - 10 features or input variables 
                - age, 
                - sex, 
                - body mass index, 
                - average blood pressure, and 
                - six blood serum measurements
                    - Total Cholesterol (TC), 
                    - Low Density Lipoprotein (LDL), 
                    - High Density Lipoprotein (HDL), 
                    - TC/HDL, 
                    - Low Tension Glaucoma (LTG) and 
                    - Glucose
            - target 
                - quantitative measure of disease progression one year after baseline.
                           
        - split into Xtrain and Xtest 
        - split into Ytrain and Ytest 
    - train a linear regression model using scikit-learn
        - call fit on the Xtrain and Ytrain 
            - regr = linear_model.LinearRegression()
            - regr.fit(diabetes_X_train, diabetes_y_train)
    - test for whether or not our implementation is correct
        - pridict on Xtest 
            - diabetes_y_pred = regr.predict(diabetes_X_test)
    - find coefficients 
        - regr.coef_
    - find cost function 
        - on Y_test and Y_predict 
        - mean_squared_error = metrics.mean_squared_error(diabetes_y_test, diabetes_y_pred)

- what is Google Colaboratory ?

- Summary
    - predict the progression of diabetes in patients ?
        - load the dataset and splitting the dataset
        - train a linear regression model using scikit-learn
        - test for whether or not our implementation is correctt)
        - find coefficients 
        - find cost function
    - what is Google Colaboratory ?
        
              
    ###############################################################################
    ## Our own implementation
     
    # train
    X = diabetes_X_train
    y = diabetes_y_train
     
    # train: initialize
    W = ...
    b = ...
     
    learning_rate = ...
    epochs = ...
     
    # train: gradient descent
    for i in range(epochs):
        # calculate predictions
        ...
     
        # calculate error and cost (mean squared error)
        ...
     
        # calculate gradients
        ...
     
        # update parameters
        ...
     
        # diagnostic output
        if i % 5000 == 0: print("Epoch %d: %f" % (i, mean_squared_error))
     
    # test
    X = diabetes_X_test
    y = diabetes_y_test
     
    # calculate predictions + calculate error and cost (same code as above)
    ...
     
    print('Coefficients: \n', W)
    print("Mean squared error: %.2f" % mean_squared_error)
    print("="*120)
     
    ###############################################################################
  
  
## Generalization and Overfitting

- generalization
    - the ability of model to perform well on new unseen data
- overfitting
    - when a model performs much better on data used to train the model, 
    - but doesn't perform well on new unseen data.
    
- Train and Test data ?
- Generalization ?
- Overfitting ?
- reason of Overfitting ?
    -  the model learns the noise present in the training data as if it was a reliable pattern.
- Overfitting  in a Regression problem ?
- graph Overfitting  in a Regression problem?
- Underfitting ?
- reason of Underfitting ? 
    - the model is unable to capture patterns present in the training data. 
- underfitting in a Regression problem ?
- graph underfitting in a Regression problem ?

- Summary
    - generalization
    - the ability of model to perform well on new unseen data
    - overfitting
    - Train and Test data ?
    - Generalization ?
    - Overfitting ?
    - reason of Overfitting ?
    - Overfitting  in a Regression problem ?
    - graph Overfitting  in a Regression problem?
    - Underfitting ?
    - reason of Underfitting ?  
    - underfitting in a Regression problem ?
    - graph underfitting in a Regression problem ?


## Strategies to Avoid Overfitting
- how to deal with overfitting ?
    - feature selection ?
        - reducing the number of features, also known as feature selection,
    - Regularization ?
        - penalize the model for having weights / parameters with large magnitude
        - modifying the cost function 
            - include a penalty term 
            - based on the magnitude of the model weights.

- parameters ?
- Hyperparameters ?
    - parameters 
        - define the model architecture
        - govern the training process
        - not estimated using machine learning algorithms 
        - are decided externally before the training process starts.
 
- what is the hyperparamater in the below polynomial regression model?
    - Ypred = w1x + w2*x**2 + w3*x**3+ ..... + wdx**d + b 
    
- What is hyperparameter tuning ?
- what is the process for selecting the best hyperparameters ?
    - We train multiple machine learning models, each with a different value for the hyperparameters. 
    - Then, we choose the best model among these.

- what is Validation Dataset ?
    - train dataset ?
    - test dataset ?
    - validation dataset ?
- Which dataset should we use for tuning the hyperparameters?

- what is  feature selection ?
- what is  Regularization ?
- how many kind of Regularization ?  
- L2 Regularization ? 
    - Ridge Regression
    - add a penalty proportional to the squared magnitude of each weight.
    - to cost function 
        - 1\n sigma(Ypred(i) - Ytrue(i))  + lamda * sigma(w(i)**2) 
    - larger weights are being penalized much more.

- L1 regularization  ?
    - Lasso Regression
    - penalty term is proportional to the absolute value of each weight
    - to cost function 
        - 1\n sigma(Ypred(i) - Ytrue(i))  + lamda * |(w(i)|
    - smaller weights are equally penalized as larger weights
    - model's parameters become sparse during optimization
    - larger number of parameters w to be zero.

- Typical machine learning workflow ?
    - Load the dataset.
    - Split the dataset into train dataset, validation dataset and test dataset. Typically, we do a 70-10-20 split.
    - Choose some specific hyperparameter values. Train the model using the train dataset. For example, we might have a linear regression model which we train using gradient descent. This gives us some values for the model parameters.
    - Evaluate the model on the validation dataset. (Not the test dataset).
    - Based on how much the model is overfitting, decide which other values for the hyperparameters we should try. Then repeat steps 3 and 4 for those hyperparameter values.
    - Choose the best model among the models we tried (based on performance on the validation data). This is our final model.
    - Test the final model on the test dataset. This tells us how well the final model performs on data it has not seen before. In particular, model parameters / hyperparameters should never be modified / chosen using the test dataset.
    - Steps 5 and 6 together are called hyperparameter tuning.

- Summary 
    - how to deal with overfitting ?
    - feature selection ?
    - Regularization ?
    - parameters ?
    - Hyperparameters ? 
    - what is the hyperparamater in the below polynomial regression model?
        - Ypred = w1x + w2*x**2 + w3*x**3+ ..... + wdx**d + b    
    - What is hyperparameter tuning ?
    - what is the process for selecting the best hyperparameters ?
    - what is Validation Dataset ?
    - Which dataset should we use for tuning the hyperparameters?
    - what is  feature selection ?
    - what is  Regularization ?
    - how many kind of Regularization ?  
    - L2 Regularization ? 
    - L1 regularization  ?
    - Typical machine learning workflow ?
        

## A Visual Introduction (and Review) of Machine Learning
- linear regression ?
    - prediction of house price 
    - elaborate with visualization ?
- naive based algorithm 
    - prediction of spam email 
    - elaborate with visualization ?
- decision tree
    - recommending app ?
    - elaborate with visualization ?
- logistic regression ?
    - acceptance at a university ?
    - based on test and grades score 
    - how to best separate the line ?
    - gradient decent 
        - log-loss function 
    - a ninja who cut the data 
        
- neural network 
    - acceptance at a university ?
    - based on test and grades score 
    - how to best separate the line ?
        - area construct on intersection of node 
        - two nodes 
        - each node lines that separated plane into two region 
        - from two node we get intersection the desired area
    - team of ninja  
- logistic regression and neural network 
- support vector machine 
    - surgon 
    - first look best part to and make a cut 
    - linear optimization 
    - find the line to maximize the distance from the boundary point 
    - linear optimization and no gradient decent 
    - the example of yellow and purple line  
    
- Kernel Trick 
    - apple up and orange down 
    - when lie is not enough ?
        - then what should the best way separate the read and green point
        
        - (0,3), (1,2), (2, 1), (3,0)
        which is one best separe teh red and green point 
        - x+y
        - xy
        - x**2
        
            (0,3), (1,2), (2,1), (3,0
       x+y  3       3       3       3
       xy   0       2       2       0
       x**2 0       1       4       9
       
       xy=0    xy=2, xy=2, xy=0
       xy=0    xy=2
       xy=0     xy=1    xy=2
                y = 1/x
       (x, y, xy) = 0,3,0   1,2,2   2,1,2   3,0,0
       
- k means clustering        
    - 3 random location for pizza parlour
    - start with random pazza store location
    - move to the center of close customer 
    - repeat the step 2-3 
    - at last will get the final location for pizza store  
    
- Hierarchical clustering 
    - if two house are close then they should serve from the same pizza store 
    
- Summary 
    - how it works ?
        - linear regression ?
        - naive based algorithm 
        - decision tree ?
        - logistic regression ?          
        - neural network ?  
        - logistic regression and neural network ?
        - support vector machine ?
        - Kernel Trick ?
        - k means clustering ?        
        - Hierarchical clustering ?
        

## Types of Learning Algorithms: Supervised, Unsupervised and Reinforcement Learning

- types of machine learning algorithm ?
    - supervised learning
        - further grouped into classification and regression problems.
            - Classification ?
            - Regression ?
    - unsupervised learning
        - goal of unsupervised learning 
        - identify patterns in the data.
        - further grouped
            - clustering 
            - association problems
    - and reinforcement learning.
    
- supervised learning ?
- Features ?
- target ?
- independent variables ?
- dependent variable
- supervised learning further grouped into classification and regression problems.
- Classification ?
    - example 
        - Spam filtering ?
        - Image classification ?
- Regression ?
    - example 
        - predict its price of a house 
        - predict rating the user is going to give to the movie 
        
- supervised learning further grouped into ?
  
- clustering 
    - discover inherent groupings in the input data 
    - example 
        - discover groups of similar users
        - for targeted marketing.
    - example 
        - Anomaly detection
        - measurements from sensors
        - identify anomalies, i.e. that something is wrong
        
- association problems
    - discover rules that describe portions of the input data
    - example 
        - Discover patterns 
        - whenever it rains, 
        - people tend to stay indoors. 
        - When it is hot, 
        - people buy more ice-cream


- Reinforcement Learning
    - the input itself depends on actions we take 
    - agent 
    - environment 
    - action 
    - reward 
    - The output depends 
        - on the state of the current input 
        - input depends on the output of the previous input.

- Semi-Supervised Learning
    -  two datasets, 
        -  one with labels and 
        -  one without
    - example
        - images and 
        - objects present in those images as training data

- Transfer Learning
    - two datasets where
        -  both of them are labelled
        -  but the type of dataset and labels are not exactly same.
    - example
        - 100 images of sentences and the corresponding labels.
        - 100,000 images of alphabets and their corresponding labels


- Classify the learning problem: 
    - Based on user’s activity (eg browsing movies Netflix), we want to group users in-order to understand the various market segments. ?
    - Predicting whether a person is male or female, given height, weight and other parameters.
    - Given previous prices and news about a company, predicting the company's stock prices at the end of next week.
    - In general, is it tougher to evaluate algorithms for supervised learning tasks (eg - classifying credit card transactions as fraudulent or not) or is it tougher to evaluate algorithms for unsupervised learning tasks (clustering banking customer without labeled data)?
    - You have to build a model for classifying trees in your locality. As you have only a limited image dataset, you tune the model developed on some large dataset for classifying trees of various types.

- Summary 
    - how many types of machine learning algorithm ?
    - what is supervised learning ?
      - what is Features ?
      - what is target ?
      - what is independent variables ?
      - what is dependent variable
      - supervised learning can be further grouped into ?
      - what is Classification problem ? give example ?
      - what is Regression problem ? give example ?
      - what is unsupervised learning ?
      - unsupervised learning further grouped into ?
      - what is clustering problem ? example of clustering ?
      - what is association problems ? example ?
      - what Reinforcement Learning algorithm ?
      - what is Semi-Supervised Learning ? example ?
      - what is Transfer Learning ? example ?
      - Classify the learning problem: 
        - Based on user’s activity (eg browsing movies Netflix), we want to group users in-order to understand the various market segments. ?
        - Predicting whether a person is male or female, given height, weight and other parameters.
        - Given previous prices and news about a company, predicting the company's stock prices at the end of next week.
        - In general, is it tougher to evaluate algorithms for supervised learning tasks (eg - classifying credit card transactions as fraudulent or not) or is it tougher to evaluate algorithms for unsupervised learning tasks (clustering banking customer without labeled data)?
        - You have to build a model for classifying trees in your locality. As you have only a limited image dataset, you tune the model developed on some large dataset for classifying trees of various types.


# Logistic Regression
- target variable is categorical and called as label 
- we can train model using gradient descent.
- we can use logistic regression 
    - for binary classification
    - to predict whether or not a person has diabetes, 
    - or whether or not an email is spam.
    
- The logistic (or sigmoid) function
    - logistic means sigmoid function
    - f(z) = 1/(1+e**-z) = e**z/(e**z + 1)
    - plot of sigmoid function ?

- the sigmoid function 
    - squashes the input value between [0, 1]
    - For large negative values, 
        - the output is very close to 0, 
    - For large positive values, 
        - the output is very close to 1.
 
- Logistic Regression Mode for binary classification
    - logistic regression equation 
        - yw,b(x)=σ(wTx+b)=exp(wTx+b)/(exp(w⊤x+b)+1)
        - wTx is w1x1 + w2x2 + ... + wkxk and b is the bias.
        
        - for example:
            - (w1, w2) = ( 2.0, -1.0)
            - (x1, x2) = (2.0, 1.0)
            - b = 1.0
            - yw,b(x) = e4.0/(e4.0+1) = 0.982
            
- In Logistic regression, yw(x) = 0.7 means that ?
    - P(y=1, x) = 0.7
    - P(y=0, x) = 0.3
    - Both options above are correct
    - None of the above
    
- Cost function
    - average negative log-likelihood function.
    - L(w)= −1/n ∑ i(y(i)true * log(yw(x(i))) + (1 − y(i)true) * log(1 − yw(x(i)))
    - example 
        - ytrue = 1 (email is spam)
        - the prediction yw(x(i)) = 0.8
        
        - L = − ( 1 × log(0.8) + 0 × log(0.2)) = − log(0.8) = 0.22
        
        - ytrue = 0
        - yw(x(i)) = 0.8
        - L = − ( 0 × log ( 0.8 ) + 1 × log(0.2 ) ) = − log ( 0.2 ) = 1.60
        
- plot 
    - plot of negative log probability
    
- You train two logistic regression models. The following is the predictions made by each model:    
    - Which model achieves lower cost?


    - algo 1        algo 2       ytrue 
    - 0.8           0.9          1
    - 0.2           0.1          0 
    - 0.9           0.5          1
    
- The cost function in logistic regression (negative log-likelihood) is always non-negative.?
- training the model i.e logistic model ?
    - gradients dL/dw 
    - Roughly speaking, the log in the cost function 'undoes' the sigmoid function.
    
- Making Predictions ?
    - calculating probability of each class
    - if yw(x) > 0.5 then the class is 1 otherwise 0.
        
- If yw(x)=0.8 , what is the value of P(y=0|x)?
    - 0.8 
    - 0.2 
    
- If yw(x)=0.8, what is the value of P(y=1|x)?
    - 0.8 
    - 0.2 
    
- True or false: Given enough time and a stable learning rate, gradient descent is guaranteed to find the optimal global solution in logistic regression.
    - true or false 
  
- In logistic regression, the gradient G for a particular weight wk is given by,

    Gwk=1/n ∑i x(i)k∗(y(i)pred−y(i)true)
    True or false: The gradient for logistic regression (and hence the update rule) is exactly the same as for linear regression.
 
   - True 
   - False 
   
- In multi-class logistic regression, the model parameters W is a matrix of size n×k
     where n is the number of classes and k is the number of input features.
    
    The probability for class c given some input x is are given as follows:
     
     P(y=c|x)=exp(WTc⋅x)/(exp(WT1⋅x)+ exp(WT2⋅x)+…+exp(WTn⋅x) )
     Here, Wm
     denotes the m-th row of weight matrix W. Notice that all the probabilities together sum to 1.
    
    Question: If there are 10 classes, WT2⋅x=5 and WTm⋅x=1
     for m∈{1,3,4,5,…,10}, then what is P(y=2|x)? (you might need a calculator).
     
    - Approximately 95%
    - Approximately 85%
    - Approximately 75%
    - Approximately 65%
    
    - solution 
        - e5/(e5+9e)≈0.85

 - Summary 
    - target variable is categorical and called as label 
    - we can train model using gradient descent.
    - we can use logistic regression 
        - for binary classification
        - to predict whether or not a person has diabetes, 
        - or whether or not an email is spam.       
    - The logistic (or sigmoid) function
    - plot of sigmoid function ?    
    - the sigmoid function 
    - prob prediction formula regression equation for binary classification ?                
    - In Logistic regression, yw(x) = 0.7 means that ?
        
    - Cost function
        - average negative log-likelihood function.
            
    - plot 
        - plot of negative log probability
        
    - You train two logistic regression models. The following is the predictions made by each model:    
        - Which model achieves lower cost?   
    
        - algo 1        algo 2       ytrue 
        - 0.8           0.9          1
        - 0.2           0.1          0 
        - 0.9           0.5          1
               
    - Making Predictions ?
        - calculating probability of each class
        - if yw(x) > 0.5 then the class is 1 otherwise 0.
            
    - If yw(x)=0.8 , what is the value of P(y=0|x)?
        - 0.8 
        - 0.2 
        
    - If yw(x)=0.8, what is the value of P(y=1|x)?
        - 0.8 
        - 0.2 
        
    - True or false: Given enough time and a stable learning rate, gradient descent is guaranteed to find the optimal global solution in logistic regression.
        - true or false 
      
    - In logistic regression, the gradient G for a particular weight wk is given by,
    
        Gwk=1/n ∑i x(i)k∗(y(i)pred−y(i)true)
        True or false: The gradient for logistic regression (and hence the update rule) is exactly the same as for linear regression.
   
            - True 
            - False 
       
    - In multi-class logistic regression, the model parameters W is a matrix of size n×k
         where n is the number of classes and k is the number of input features.
        
        The probability for class c given some input x is are given as follows:
         
         P(y=c|x)=exp(WTc⋅x)/(exp(WT1⋅x)+ exp(WT2⋅x)+…+exp(WTn⋅x) )
         Here, Wm
         denotes the m-th row of weight matrix W. Notice that all the probabilities together sum to 1.
        
        Question: If there are 10 classes, WT2⋅x=5 and WTm⋅x=1
         for m∈{1,3,4,5,…,10}, then what is P(y=2|x)? (you might need a calculator).
         
        - Approximately 95%
        - Approximately 85%
        - Approximately 75%
        - Approximately 65%
        
        - solution 
            - e5/(e5+9e)≈0.85

## K-nearest neighbors
    
- It is a supervised learning algorithm which can be used for both classification and regression.
- there is no explicit training phase in KNN
    - for classifying new data points, 
        - directly use our dataset 
        - in some sense, the dataset is the model.
    
- Understanding the classification algorithm (illustration)
    - For simplicity of visualization
        - input data has 2 dimensions 
        - it is a binary classification task 
            - target can take two possible labels - green and red.
- how KNN works for the classification and the regression problem ?
- what are the hyper parameter for the KNN ?

- Tuning the hyper-parameter K
    - K is small
        - consider a few close neighbors
    - K grows large
        - consider more neighbors
        -  even far off points
        
    - when K = N  (the total number of data points
        - all points in the training data, considered neighbors
        - same prediction always 
            
    - hyper-parameter tunin
        - pick a good value of K
        - using the validation dataset.    
        - k=√n (square root of total number of data points).
        
    - this will improve the accuracy 
        - by reducing overfitting.
        
- Distance metrics
    - options available for distance metric
        - euclidean or
            - sqrt(sigma((xi - yi)**2))
        - manhattan distance
            - mod(sigma(xi - yi))
        - minkowski 
            - (sigma(|x1-y1|**q))**1\q
        
- Importance of feature scaling / data normalization
    - we could make sure that for each dimension, 
    - the mean is 0 mean and 
    - standard deviation is 1.
    
    - Otherwise, 
        - the distance metric will give 
            - more importance to some dimension 
            - as opposed to other dimensions.
            
    - Let's take a simplified example to understand this.
     
    - Here's the formula 
        - for the euclidean distance between the data points 
        - x(1) and x(2).
        
        - √(x(1)1−x(2)1)2+(x(1)2− x(2)2)2
        
        - In this simple example, 
            - let's say that x1 varies between -1 to +1 and 
            - x2 varies between -1000 to +1000. 
            
            - It is very clear that  
                - x2 will be responsible for most of the total distance,
                - whereas x1 will have almost no impact.
                
    - what is scaling ? 
    
- Parametric
    - finite set of parameters
        - help predict the target value for any test data
        - training takes time
        - its really quick to predict the target value for test data.
    - From the given train data
        - it learns the values of these parameters
        
    - Benefits of Parametric Models are:
        - Less Data
        
    - Limitations of Parametric Models are:
        - more suited to simpler problems
        - methods constrained to that specified functional form
        
- Non-parametric models
    - example: KNN
    - training phase is quick or negligible
    - no parameters/infinite parameters to learn
    - training is quick compared to prediction
    
    -  a new test data point arrives
        - algorithm then  
            - compares with the train data 
            - to predict the target value
            
    - Benefits of Non-parametric Models are:
        - capable of fitting a large number of functional forms
        - No assumption
     
    - Limitations of Non-parametric Models
        - lot of training data
        - overfitting the training data   
        
- Summary
    - K-nearest neighbors (KNN) is a supervised learning algorithm which can be used for both classification and regression.
    - KNN predict the class which is most common among the K closest data points for classification problems.
    - In case of regression, when target variable is a real value, KNN takes the average of the K nearest neighbors.
    - K is a hyper parameter which is tuned to select the best value.
    - Always perform feature scaling / normalization on the data points before using them in KNN. 
   
    - there is no explicit training phase in KNN
        - for classifying new data points, 
            - directly use our dataset 
            - in some sense, the dataset is the model.
    - Understanding the classification algorithm (illustration)
        - For simplicity of visualization
            - input data has 2 dimensions 
            - it is a binary classification task 
                - target can take two possible labels - green and red.
    - how KNN works for the classification and the regression problem ?
    - what are the hyper parameter for the KNN ?
    - Tuning the hyper-parameter K            
    - Distance metrics         
    - Importance of feature scaling / data normalization                    
    - what is scaling ?       
    - Parametric VS Non-parametric models
   
   
## Hands-on Project: Digit classification with K-Nearest Neighbors and Data Augmentation
- K-Nearest Neighbors algorithm to handwritten digit classification
- main objectives are
    - experiment with various hyper-parameters,
    - introduce metrics classification accuracy
    - confusion matrix
    - develop intuition about how KNN works
    - use this intuition and data-augmentation to improve classification accuracy further.
   
 - https://www.dropminkowskibox.com/s/dehe51jtysxhpor/mnist_10000.pkl.gz?dl=1
 - https://www.dropbox.com/s/d3hz2dli4z6imfl/mnist_1000.pkl.gz?dl=1
 
 
 
## Understanding Confusion Matrix, Precision-Recall, and F1-Score
 
 - https://towardsdatascience.com/understanding-confusion-matrix-precision-recall-and-f1-score-8061c9270011
 
 - accuracy shouldn’t be the only performance metric you care about while evaluating a Machine Learning model
 
 - Confusion Matrix
    - also known as an error matrix
    - specific table 
    - allows visualization of the performance of an algorithm
    - a supervised learning one
    - the goal of evaluating a model using the confusion matrix is
        - maximize the values of 
            - TP and TN 
        - minimize the values of 
            - FP and FN.
        
      ```
            <---------------Actual----------------->
            
            ^               True                False 
            :
            :
            :   Positive     True                False
        Predicted          Positive            Positive     
            :
            :   
            :   Negative    True                False                
            :               Negative            Negative  
      ```  
        
    
 - How does the confusion matrix help detect overfitting?
    - skewed dataset i.e 
        - one of the target variables will be having more data points 
        - than the other.
        
        
 -  confusion matrix along with the accuracy
    - clearly identify that the model is overfitting on the training dataset
    - it is predicting every unknown data point as a patient not having heart disease.
    
 
 - Precision-Recall
    - accuracy can be misleading in some cases.
    - (also called positive predictive value)
    - fraction of relevant instances among the retrieved instances
    - percentage of the positive predictions made were actually correct.
    
    - PRECISION = True Positive (TP)/ ((True Positive ) + False Positive)
     
    
 - Recall
    - also known as sensitivity
    - fraction of the total amount of relevant instances that were actually retrieved
    - percentage of actual positive predictions were correctly classified by the classifier.
     
    - Recall = True Positive / (True Positive + False Negative)  
    
 - Precision-Recall Trade-Off
    - high precision and low recall.
    - Higher precision 
        - because now the classifier is more confident 
        - that the patient has heart disease
        
    - Lower recall
        - because now that the classifier’s threshold is set so high, 
        - there will be fewer patients classified 
        - as having heart disease.
        
    - threshold to 0.7 or 0.8
    
 - F1-Score
    - Precision-Recall values 
    - understand performance of a specific algorithm
    - is a measure of a test’s accuracy.
    - ranges between 0 and 1.
    
    - F1 Score = 2 * (Precision * Recall) / (Precision + Recall) 
    
 - some other evaluation metrics like 
    - Log loss, 
    - ROC-AUC curve, 
    - Categorical Crossentropy, 
    - and more.
    
  
 - Which digit is the most confusing for the model? (lowest f1-score)
    - 0 
    - 3
    - 5
    - 8
    
 - Which digit does the model confuse images of 4 with? That is, when it is incorrect, what does the model predict most often, given that the image is a 4?
    - 1
    - 7
    - 9 
    
 - Which digit does the model confuse images of 7 with? That is, when it is incorrect, what does the model predict most often, given that the image is a 7?
    - 1
    - 4
    - 9 
    
 - Which of these best describes what a KNN is doing?
    - Using each training image as a template, and checking how well the current image matches
    - Generating one template per digit, and checking how well the current image matches
   
 - Which of these best describes what a (multi-class) logistic regression model would do for the current problem?
    - Using each training image as a template, and checking how well the current image matches
    - Generating one template per digit, and checking how well the current image matches
    
    
 - Which of these best describes what a (multi-class) perceptron would do for the current problem?
    - Using each training image as a template, and checking how well the current image matches
    - Generating one template per digit, and checking how well the current image matches

 - Would you expect the KNN model to perform well if it got an image of the digit 1 from the test data, but the digit was shifted by 8 pixels to the right?
    - Yes
    - No 
    
    - explanation 
        - if the digit was shifted to the right, the KNN would still compare each pixel to each pixel. But now, the pixels where the 1 is positioned has been moved substantially. Hence, the euclidean distance would be high between this shifted image and the normal images of digit 1.
        - Data Augmentation: Since the original data would not have perfect centering for all the images, we can create more training data as follows. For each image, we create 4 more images, where each new image is the original image shifted by 1 pixel in each direction. For example, to shift it right by 1 pixel, we delete the last column, and add a white column as the first column. Now, we have 5x the training data for the KNN, and you should see the accuracy go up (the computation time will also go up). You can similarly add 2 pixel shifts.

- Summary 
    - Confusion Matrix ?
    - visualisation table ?
    - the goal of evaluating a model using the confusion matrix is
    - How does the confusion matrix help detect overfitting?
    - skewed dataset ?
    -  confusion matrix along with the accuracy
    - Precision ?
    - formula of PRECISION ?
    - Precision also know as ?
    - Recall
    - also known as ?
    - formula of Recall ?
    - high precision and low recall ? 
    - F1-Score
    - Precision-Recall values 
    - F1-Score ranges between ?
    - formula of F1-Score ?
    - some other evaluation metrics like 
        - Log loss, 
        - ROC-AUC curve, 
        - Categorical Crossentropy, 
      
    - Which digit is the most confusing for the model? (lowest f1-score)
        - 0 
        - 3
        - 5
        - 8
        
    - Which digit does the model confuse images of 4 with? That is, when it is incorrect, what does the model predict most often, given that the image is a 4?
        - 1
        - 7
        - 9 
        
    - Which digit does the model confuse images of 7 with? That is, when it is incorrect, what does the model predict most often, given that the image is a 7?
        - 1
        - 4
        - 9 
        
    - Which of these best describes what a KNN is doing?
        - Using each training image as a template, and checking how well the current image matches
        - Generating one template per digit, and checking how well the current image matches
    
    - Which of these best describes what a (multi-class) logistic regression model would do for the current problem?
        - Using each training image as a template, and checking how well the current image matches
        - Generating one template per digit, and checking how well the current image matches
        
    - Which of these best describes what a (multi-class) perceptron would do for the current problem?
        - Using each training image as a template, and checking how well the current image matches
        - Generating one template per digit, and checking how well the current image matches
        
    - Would you expect the KNN model to perform well if it got an image of the digit 1 from the test data, but the digit was shifted by 8 pixels to the right?
        - Yes
        - No 
        
        - explanation 
            - if the digit was shifted to the right, the KNN would still compare each pixel to each pixel. But now, the pixels where the 1 is positioned has been moved substantially. Hence, the euclidean distance would be high between this shifted image and the normal images of digit 1.
            - Data Augmentation: Since the original data would not have perfect centering for all the images, we can create more training data as follows. For each image, we create 4 more images, where each new image is the original image shifted by 1 pixel in each direction. For example, to shift it right by 1 pixel, we delete the last column, and add a white column as the first column. Now, we have 5x the training data for the KNN, and you should see the accuracy go up (the computation time will also go up). You can similarly add 2 pixel shifts.

## Support Vector Machine (SVM)

- is a classification algorithm
- used for binary classification
- SVM with gaussian kernel
    - best machine learning models
    - highest accuracy
        - for large variety of datasets
        - which do not involve images or audio.
- Understanding the various hyper-parameters 

- Max-margin classifier
    - SVM algorithm seeks the hyperplane with the largest margin
        - it maximizes the distance between 
            - the hyperplane and 
            - the nearest data point.
    - known as a maximum-margin classifier
    
    - support vectors
        - The closest data points are called .
        
    - the red line 
        - is the maximum-margin classifier, 
        
    - and the data points marked in the yellow 
        - are the corresponding support vectors.
        
    - Given a binary classification dataset
        - SVM aims to find a separating hyperplane between 
        - positive and negative instances.
        
        - For example, 
            - the blue lines is a plane 
            - which separates the positive and negative data points. 
            
            - All blue points lie on one side, 
            - and all red points lie on the other rise. 
            
            - New unseen samples are categorized 
                - based on the side of the hyperplane 
                - in which they fall.
                
    - there are many possible hyperplanes 
        - that separate all data points correctly

- Intuition behind maximum-margin
    - reason for preferring the max-margin classifier over other hyperplanes
        - expected to perform better on test data / unseen data.
        
    - expect classifier A 
        - to perform better than classifier B, 
        - since classifier B is very close to one the green points.
        
- What if data is overlapping? Soft-margin
    - SVM algorithm includes a regularization parameter 
        - (usually denoted as C), 
        - which controls 
            - how important 
            - it is to avoid misclassifying data points.
            
    - Large values of C 
        - SVM classifier classify more points correctly
        
    - small values of C 
        - data points might be misclassified, 
        - but resulting classifier 
            - have a larger margin 
            - with the rest of the data points.
            
    - if the dataset has more outliers,
        - more data points to be misclassified.
        
    - You find that your SVM model is overfitting the data points. Should you try increasing the hyper parameter C or decreasing it?
        - Increasing
        - Decreasing
        
        - C controls how important it is to avoid misclassifying data points. So decreasing C reduces overfitting.
        
    - SVM produces discrete values (for classification) while logistic regression produces probabilistic values.
        - True 
        - False
        
- What if optimal classifier is not linear? Kernel trick ?
    - limitation of an SVM 
        - (even with soft-margin) 
        - classification boundary is linear. 
        
    - benefit of Kernel trick ?
        - introducing new dimensions, 
        - mapping the data to a higher dimension
        - possible to separate the data with a linear hyperplane.
        - R^3
        - visualization in 
            - 2D
            - 3D
        
        - For example,
            - define a third dimension z
            - z =x**2+y**2
            - the learnt decision boundary is a circle
            - projecting the data to a higher dimension 
                - allows us to learn classifiers 
                    - which are non-linear with respect 
                    - to the original data.   
                                   
- Kernel trick in practice ?
    - two more important things about SVM kernels
        - perform the transformation on the data implicitly.
            - need not go through every data point and 
            - calculate the values for the new dimensions
        - figured out a set of kernels
            - work very well on a large number of datasets
            
    - radial basis function
        - polynomial kernel 
        - and gaussian kernel 
        
- Adjusting the number of support vectors: Gamma
    - provides 
        - regularization parameter C 
        - choice of kernel and 
        - third powerful hyper-parameter γ(gamma)
        
    - third powerful hyper-parameter γ(gamma)
        - how far the influence of each data point.
        - high values of γ
            - imply a small influence and 
            - hence a small number of support vectors. 
        - Whereas low values of γ
            - imply a large influence and 
            - hence a large number of support vectors.
            
- Multiclass SVM
    - to be used on
        - binary classification model
        - multi-class dataset 
        
    - One-versus-rest
        - winner-takes-all strategy
        - train one SVM classifier per class   
            - to distinguish one single class from the rest.
        - new instances will be categorized based 
        - on the highest scoring output.
        
    - One-versus-one
        - max-wins voting strategy
        - train one SVM classifier for each pair of classes
        - That is, each classifier 
            - assigns the instance to one of the two classes, 
            - and the final prediction 
                - is given by the class with the most votes. 
                
    - If you are dealing with 5 class classification problem and you want to use the One-versus-rest method. How many SVM models do we need to train in this case?
        - 1
        - 3
        - 5
        - 10 
                
    - If you are dealing with 5 class classification problem and you want to use the One-versus-one method. How many SVM models do we need to train in this case?
        - 1
        - 3
        - 5 
        - 10 
        - All possible combinations of 2 classes at a time out of 5 classes gives 10. ( 5C2 = 10).
       
- Putting things together with code
    - arguments
        -   class sklearn.svm.SVC(
            - C=1.0, 
            - kernel=’rbf’, 
            - degree=3, 
            - gamma=’auto’, 
            - coef0=0.0, 
            - shrinking=True, 
            - probability=False, 
            - tol=0.001, 
            - cache_size=200, 
            - class_weight=None, 
            - verbose=False, 
            - max_iter=-1, 
            - decision_function_shape=’ovr’, 
            - random_state=None) 
            
    - Model hyper-parameters
        - C 
            - regularization parameter 
            - controls how important it is to avoid 
            - misclassified data points
        - kernel = 'rbf' 
            - radial basis function, i.e. Gaussian kernel
        - kernel = 'poly'
            - 'poly' (polynomial kernel) are most popular
        - degree 
            - only applicable for polynomial kernel.
        - gamma 
            - controls the range of influence of each data point  
            
    - Stopping criterion for training
        - tol 
            - tolerance for stopping
        - max_iter 
            - hard limit for number of iterations
            
    - Multiclass SVM:
        - decision_function_shape = 'ovo' 
            - (one-versus-one)  
        - decision_function_shape = 'ovr'  
            - 'ovr' (one-versus-rest)
- Applications
    - models for a large variety of datasets 
    - non-media datasets
    
- Summary 
    - classification is used for ?
    - svm with gaussian kernel not good for ?
    - svm with gaussian kernel gives ?
    - what is Max-margin classifier ?
    - what are support vectors ?
    - Given a binary classification dataset SVM aims to find ?
    - what is the Intuition behind maximum-margin ?
    - what if regularization parameter (c) is large  ?
    - what if regularization parameter (c) is small  ?
    - You find that your SVM model is overfitting the data points. Should you try increasing the hyper parameter C or decreasing it?
        - Increasing
        - Decreasing
    - SVM vs logistic regression
    - SVM produces discrete values (for classification) while logistic regression produces probabilistic values.
        - True 
        - False
    - limitation of an SVM ?
    - what is Kernel trick ?
    - benefit of Kernel trick ?     
    - SVM Kernel trick perform the transformation on the data ?.
    - what are radial basis function ?
    - use of C, Kernal, gamma ?
    - what are Multiclass SVM ?
    - what is One-versus-rest ?
    - what is One-versus-one ?
    - If you are dealing with 5 class classification problem and you want to use the One-versus-rest method. How many SVM models do we need to train in this case?
        - 1
        - 3
        - 5
        - 10 
                
    - If you are dealing with 5 class classification problem and you want to use the One-versus-one method. How many SVM models do we need to train in this case?
        - 1
        - 3
        - 5 
        - 10 
        - All possible combinations of 2 classes at a time out of 5 classes gives 10. ( 5C2 = 10).
    
    - arguments
        -   class sklearn.svm.SVC(
            - C=1.0, 
            - kernel=’rbf’, 
            - degree=3, 
            - gamma=’auto’, 
            - coef0=0.0, 
            - shrinking=True, 
            - probability=False, 
            - tol=0.001, 
            - cache_size=200, 
            - class_weight=None, 
            - verbose=False, 
            - max_iter=-1, 
            - decision_function_shape=’ovr’, 
            - random_state=None) 
            
    - Model hyper-parameters
        - C 
            - regularization parameter 
            - controls how important it is to avoid 
            - misclassified data points
        - kernel = 'rbf' 
            - radial basis function, i.e. Gaussian kernel
        - kernel = 'poly'
            - 'poly' (polynomial kernel) are most popular
        - degree 
            - only applicable for polynomial kernel.
        - gamma 
            - controls the range of influence of each data point  
    - Stopping criterion for training ?
        - tol 
        - max_iter 
    - Multiclass SVM ?
        - decision_function_shape = 'ovo'  ?
        - decision_function_shape = 'ovr'   ?
    - Applications ?


- Learning = Representation + Evaluation + Optimization
    - representation (what the model looks like), evaluation (how do we differentiate good models from bad ones), and optimization (what is our process for finding the good models among all the possible models)


## What is deep learning? How does it relate to machine learning?
- subfield of machine learning
- learning models have 
  - a notion of multiple layers
  - multiple levels of hierarchy,
- which opens 
  - the possibility to learn models 
  - for more complicated tasks.

-  multiple levels of hierarchy,
   - Each successive layer uses the output from the previous layer as input.
   - multiple levels 
     - of features
     - of representations of the data
   - based on 
     - the (unsupervised) learning
   - Higher level features 
     - are derived from lower level features 
     - to form a hierarchical representation.

- Examples for what we mean by levels of hierarchy
  - books are made of chapters, 
  - chapters are made of paragraphs, 
  - paragraphs are made of sentences, 
  - sentences are made of words, 
  - words are made of characters.

- Relation to traditional machine learning
  - feature extraction.


- A First Look at Neural Networks
  - 
  - Neurons
    - basic foundational unit 
    - Each neuron 
      - has a set of inputs
      - given a specific weight
    - computes some function 
      - on these weighted inputs
  
  - sigmoidal neuron
    - feeds the weighted sum of the inputs 
    - into the logistic function
  - linear neuron
    - linear combination of the weighted inputs  
  - weighted sum is very negative
    - the return value is very close to 0
  - weighted sum is very large and positive
    - return value is very close to 1
  - logistic function
    -  it introduces a non-linearity
    -  to enable the neural network to learn more complex models
  - in absence of these non-linear functions 
    - (called activation functions),
    - the entire neural network would be a linear function, and the layers would not be useful.   
  - feed-forward neural networks 
  - Some important notes
    - inputs and outputs are vectorized representations

## Algorithms for Unsupervised Learning
- Unsupervised learning is a 
  - set of techniques 
  - to identify patterns and 
  - underlying characteristics in data.
  
- K-Means Clustering
  - unsupervised learning. 
  - algorithm to perform clustering
  - use case 
    - data mining, 
    - pattern recognition and 
    - anomaly detection.
  - partitions the dataset cluster centroids
  - relocating the K centroids
  - re-classifying data points.

- Initialization position of the centroids
  - Forgy method
    - Centroids are randomly chosen
  - Kmeans++
    - Centroids are selected 
    - to be as far from each other as possible, 
    - with the goal of converging faster. 
    - They may or may not correspond to existing data points.
  
- Minimization problem
  - min cj K∑j=1 ∑xi∈Cj ||xi−cj||2,
  - algorithm typically uses the Euclidean distance.

- How do you choose K?  
  - Rule of thumb:
    - K ≈ sqrt(N/2).
    - each cluster will ideally have sqrt(2N) points.
    - For example
      - K = sqrt(50/2) = 5
      - data points = sqrt(2* 50) = 10 
  
  - Elbow method: 
    - clustering for a range of parameter value
    - calculate the total within-cluster sums-of-squares
  
  - Silhouette analysis
    - By studying the separation distance between clusters.
    
- Soft assignment
  - each data point belongs to different clusters with different probabilities.
