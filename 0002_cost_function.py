import importlib

from v1_dataset import dataset

prediction_function = importlib.import_module('0001_prediction_function')


def cost_function(w1, b):
    """
    calculate cost given model parameters.
    # make prediction for each data (x, y_true) and calculate squared error

    :param w1:
    :type w1:
    :param b:
    :type b:
    :return:
    :rtype:
    """
    squared_errors = list()
    for x, y_true in dataset:
        y_pred = prediction_function.predict(x, w1, b)
        squared_error = (y_pred - y_true) ** 2
        squared_errors.append(squared_error)
    # return average of squared_errors
    return sum(squared_errors) / len(squared_errors)


if __name__ == "__main__":
    b = 15.0  # bias
    w1 = 3.0  # weight
    print(cost_function(w1, b))
