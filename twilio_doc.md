#### Dynamic Call Center with Python and Django
- automate the routing of calls from customers to your support agents. 
    - customers would select a product
    - connected to a specialist for that product
    - If no one is available our customer's number will be saved so that our agent can call them back.
    
- Configure a workspace using the Twilio TaskRouter REST API.
- Listen for incoming calls and let the user select a product with the dial pad.
- Create a Task with the selected product and let TaskRouter handle it.
- Store missed calls so agents can return the call to customers.
- Redirect users to a voice mail when no one answers the call.
- Allow agents to change their status (Available/Offline) via SMS.

- Configure the Workspace
    - instruct TaskRouter to handle the Tasks
        - TaskRouter Console
        - programmatically using the TaskRouter REST API.
        
- 
- 
