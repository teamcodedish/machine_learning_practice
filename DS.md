## Why do we need (efficient) algorithms?
- Introduction
    - estimate the run-time approximately.
- Rate of growth
    - focus on estimating the execution time as a function of the inputs.
- Big-O notation
     - function1 as O(n2), 
     - function2 as O(n) and 
     - function3 as O(1).
     
     
    - def function1(n):
        a = 0
        for (i = 0; i < n; i += 1):
            for (j = 0; j < n; j += 1):
                a += 1
        return a
        
    - def function2(n):
        a = 0
        for (i = 0; i < n; i += 1):
            a += n
        return a
        
    - def function3(n):
        return n * n
    