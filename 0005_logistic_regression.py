# loading iris dataset, contains information about iris flowers.

import numpy as np
from sklearn import datasets

# load iris dataset
iris = datasets.load_iris()
X = iris.data[:, :]

# original dataset has 4 classes. We make y binary.
y = (iris.target != 0) * 1

# 4 input features in the dataset
# 4 types in the target
# we will only classify whether the flower is Type 0 or not.
print(X[:10, :])
print(y[:10])

# split the dataset into train and test

# random shuffle
a = np.arange(150)
np.random.seed(42)
np.random.shuffle(a)
test = list(a[0:30])
train = list(a[30:150])

# train/test split
X_train = X[train]
X_test = X[test]

y_train = y[train]
y_test = y[test]


# write all the helper functions we need for logistic regression:
def zed(W, b, x):
    # Computes the weighted sum of inputs
    return x.dot(W) + b


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def prob_prediction(W, b, x):
    # Returns the probability after passing through sigmoid
    return sigmoid(zed(W, b, x))


def predict(W, b, x):
    prob = prob_prediction(W, b, x)
    return prob >= 0.5


def cost_func(h, y):
    return (-y * np.log(h) - (1 - y) * np.log(1 - h)).mean()


# accuracy evaluation
def evaluate(h_test, y):
    return (h_test == y).mean() * 100


# perform the training and testing

# initialization
W = np.random.uniform(low=-0.1, high=0.1, size=X_train.shape[1])
b = 0.0
learning_rate = 0.1
epochs = 20000

# training: fit the model to training set
print('Training:')
for i in range(epochs):
    # calculate predictions
    y_predict = prob_prediction(W, b, X_train)

    # calculate error and cost (mean squared error)
    cost = cost_func(y_predict, y_train)

    # calculate gradients
    W_gradient = (1.0 / len(X_train)) * (y_predict - y_train).dot(X_train)
    b_gradient = (1.0 / len(X_train)) * np.sum(y_predict - y_train)

    # diagnostic output
    if i % 1000 == 0: print("Epoch %d: Cost = %f" % (i, cost))

    # update parameters
    W = W - (learning_rate * W_gradient)
    b = b - (learning_rate * b_gradient)

# evaluation
print('Testing:')
test_set_predictions = predict(W, b, X_test)
print('Accuracy percentage:', evaluate(test_set_predictions, y_test))
