# KNN implementation

## load the dataset
from sklearn.datasets import load_iris

dataset = load_iris()

# X - independent variables
X = dataset.data

# y - target variable
y = dataset.target

# Dataset before scaling
print('Dataset before scaling')
print('X_before_scaling (first 5 rows)')
print(X[:5, :])

## pre-processing
# standardize the data to make sure each feature contributes equally
# to the distance

from sklearn.preprocessing import StandardScaler

ss = StandardScaler()
X_processed = ss.fit_transform(X)

# Dataset after scaling
print('Dataset after scaling')
print('X_processed (first 5 rows)')
print(X_processed[:5, :])

# Print first 5 values of y
print('y (first 5 values)')
print(y[:5])

## split the dataset into train and test set
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_processed, y,
                                                    test_size=0.3,
                                                    random_state=42)

## fit n nearest neighbor model
from sklearn.neighbors import KNeighborsClassifier

model = KNeighborsClassifier(n_neighbors=5, metric="minkowski", p=2)
# p=2 for euclidean distance

# fit the model with train set
model.fit(X_train, y_train)
print('\nDisplay all arguments scikit-learn is using for the KNN model')
print(model)

## evaluate
score = model.score(X_test, y_test)
print('\nScore:', score)
