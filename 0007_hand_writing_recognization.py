# Loading the dataset

import gzip
import pickle

f = gzip.open('mnist_10000.pkl.gz', 'rb')
trainData, trainLabels, valData, valLabels, testData, testLabels = pickle.load(
    f, encoding='latin1')
f.close()

# trainData is a NumPy array with shape (10000, 784)
# which are the values for the pixels of the 28 x 28 image (arranged row-by-row)
# A pixel value of 0.0 denotes white (background)
# pixel value of 1.0 denotes black (foreground)
# Values in between denote the pixel intensities.
print("training data points: {}".format(len(trainLabels)))
print("validation data points: {}".format(len(valLabels)))
print("testing data points: {}".format(len(testLabels)))

# Looking at the images
# pip3 install opencv-python

# to see the training images
import cv2

image = trainData[0]
image = image.reshape((28, 28))
cv2.imshow("Image", image)

# Choosing the best hyperparameters
# install sklearn
# use sklearn package's KNeighborsClassifier
# Results from the following tasks will be asked in the quiz.
# Use Euclidean distance for all tasks

# implement K-nearest neighbors for a number of values of k
# measure the accuracy for those values:

# Task 1: Try the following values of K,
# and note the classification accuracy on the validation data
# for each. K = 1, 3, 5, 9, 15, 25


from sklearn.neighbors import KNeighborsClassifier

best_k = 0
best_score = 0

lst = [1, 3, 5, 9, 15, 25]
# lst = [25]
for k in lst:
    model = KNeighborsClassifier(n_neighbors=k)
    model.fit(trainData, trainLabels)
    score = model.score(valData, valLabels)
    print(k, score)
    if score > best_score:
        best_k, best_score = k, score
        print(best_k, best_score)

# Task 2: For the best performing value of K,
# calculate and note the classification accuracy on the test data.
model = KNeighborsClassifier(n_neighbors=best_k, metric='euclidean')
model.fit(trainData, trainLabels)
score = model.score(testData, testLabels)
predictions = model.predict(testData)

# Inspect the performance per class,
# i.e. precision, recall and f-score for each digit
# http://scikit-learn.org/stable/modules/generated/sklearn.metrics.classification_report.html
from sklearn.metrics import classification_report

print("classification_report(testLabels, predictions)")
print(classification_report(testLabels, predictions))

# Task 4: Inspect the confusion matrix,
# i.e. when the correct label was digit I,
# how times did the model predict J.
# (hint: see sklearn.metrics.confusion_matrix)
# http://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html

from sklearn.metrics import confusion_matrix

print(confusion_matrix(testLabels, predictions))

# Improving accuracy further with Data Augmentation
# try difference distance metrics
